import './App.css';
import React,{useEffect} from 'react';
import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import { DataProvider } from "./common/DataContext/index";
import { Main } from "./pages/Main";
import { About } from "./pages/About";
import { Projects } from "./pages/projects";
import { Services } from "./pages/Services";
import { Contact } from "./pages/contact";
import { MapPage } from "./pages/map";
import {fetcher,poster} from './calls/calls'
function App() {
  useEffect(() => {
    document.title = "Gladis Geotech"
  }, [])




  return (
    // <div class= 'appDiv'>
    <>
      <BrowserRouter>
        <Routes>
          <Route exact path ="/" element={<Main />}/>
          <Route exact path ="/about" element={<About />}/>
          <Route exact path ="/map" element={<MapPage />}/>
          <Route exact path ="/projects" element={<Projects />}/>
          <Route exact path ="/services" element={<Services />}/>
          <Route exact path ="/contact" element={<Contact />}/>
        </Routes>
      </BrowserRouter>
      </>
    // </div>
  );
}

export default App;
