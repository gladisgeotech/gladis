import { Button } from "bootstrap";
import React,{useState} from "react";
import searchButton from '../../static/search_icon.png';
import layerButton from '../../static/layerIconWhite.png';
import closeButton from '../../static/close_button.png';
import polygonButton from '../../static/polygon_button.png';
import { RadioGroup,Radio } from "react-radio-group";
import './styles.css'
export const SearchButton=(props)=>{
  return(
    <>
      <button
        style={{
          width:'50%',
          marginBottom:'0vh'
        }}
        onClick={props.action}
      >
        {props.text}
      </button>
    </>
  )
}

export const SearchField=(props)=>{
  return(
  <>
    <input 
    value={props.text}
    placeholder={props.placeholder}
    style={{width:'95%',marginBottom:'1vh'}}
    type={'text'}
    onChange={(e)=>{props.setter(e.target.value)}}
    />
  </>
  )
}

export const SearchResultListItem=(result)=>{
  return(
    <div style={{display:'flex',flexDirection:'row',marginBottom:'1vh'}}>
     <label> 
      {result.name}
     </label> 
    </div>
  )}

export const SearchResultList=(props)=>{
  return(
    <div  style={{marginTop:'0',height:'25vh',overflow:'scroll'}}>
      {props.searchResults.map((row)=>{
        const{
          index,
          name,
          coordinates,
        }=row;
        return(
        <div key={index} style={{display:'flex',flexDirection:'row',marginBottom:'1vh'}}>
        <input
        key={index}
        type={"radio"}
        value={index}
        checked={props.resultSelected===index}
        onChange={()=>props.handleSetResultSelected(index)}
        />
        <label> 
         {name}
        </label> 
       </div>
        )
      })}
    </div>
  )
}


export const LayerWidget=(props)=>{
  return(
  <div style={{          
    position:'absolute',
    top:'1%',
    left:'2%',
    }}
    >
      {props.layerOpen?
      <div 
      class="layerGroup"
        >
      <RadioGroup 
      selectedValue={props.selectedLayer}
      onChange={props.handleLayerChange}
      onClick={props.handleSetLayerOpen}
      >
        <Radio key ={0}value="mapbox://styles/mapbox/streets-v11?optimize=true" />Street
        <Radio key ={1} value="mapbox://styles/mapbox/light-v10?optimize=true" />Light
        <Radio key ={2} value="mapbox://styles/mapbox/dark-v10?optimize=true" />Dark
        <Radio key ={3} value="mapbox://styles/mapbox/satellite-v9?optimize=true" />Sat      
      </RadioGroup>
      </div>
      :
      <>
      <img style={{
        width:'40px',
        }}
        src={layerButton}
        onClick={props.handleSetLayerOpen}
        />
      </>
    }
  </div>
  )
}

export const SearchWidget=(props)=>{
    return(
  <div style={{          
    position:'absolute',
    top:'8%',
    left:'2%',
    height:'100%'
    }}
    >
    {props.searchOpen?
      <div 
      class="searchGroup"
        >
        <h3>Search locations by any or all of the fields below</h3>
        <SearchField 
        text={props.address}
        setter={props.handleSetAddress}
        placeholder={'Address & Street Name'}
        />
        <SearchField 
        text={props.city}
        setter={props.handleSetCity}
        placeholder={'City'}
        />
        <SearchField 
        text={props.postcode}
        setter={props.handleSetPostcode}
        placeholder={'Postcode'}
        />
        <SearchField 
        text={props.state}
        setter={props.handleSetState}
        placeholder={'State'}
        />
        <SearchField 
        text={props.country}
        setter={props.handleSetCountry}
        placeholder={'Country'}
        />
        <div style={{display:'flex',flexDirection:'row',marginBottom:"1vh"}}>
        <SearchButton 
        text={"Close"}
        action={props.handleSetSearchOpen}
        />
        <span style={{width:'1vw'}}/>
        <SearchButton 
        text={"Clear"}
        action={props.handleClearSearch}
        />
        <span style={{width:'1vw'}}/>
        <SearchButton 
        text={"Search"}
        action={props.action}
        />
        <span style={{width:'1vw'}}/>
        <SearchButton 
        text={"View"}
        action={props.handleViewResult}
        />
        </div>
        {props.resultsOpen?
        <>
        <SearchResultList
          searchResults={props.searchResults}
          handleSetResultsOpen={props.handleSetResultsOpen}
          resultSelected={props.resultSelected}
          handleSetResultSelected={props.handleSetResultSelected}
          handleClearSearch={props.handleClearSearch}
        />
        </>
        :
        <></>
        }
      </div>
      :
      <>
      <img style={{
      width:'40px',
      }}
      src={searchButton}
      onClick={props.handleSetSearchOpen}
      />

      </>
}
      


  </div>
    )
  }

export const DrawPolygonWidget =(props)=>{
return(
  <div style={
    {position:'absolute',top:props.polyWidgetTop,left:'2%',height:'100%'}
  }
    >
  {!props.drawPolyOpen?
  <img style={{
    width:'40px',
    }}
    src={polygonButton}
    onClick={props.handleSetDrawPolyOpen}
  />
  :
  <>
  <div 
    class="drawPolyGroup"
  >
    <h3 style={{textAlign:'center'}}>Draw Polygon</h3>
    <h4 style={{textAlign:'center'}}>
      Draw three or more points & press "Set" to get boundary coordinates, area and centroid of the polygon.
    </h4>
    <SearchField 
    text={props.displayBoundary}
    // setter={props.handleSetPostcode}
    placeholder={'Boundary:'}
    />
    <SearchField 
    text={props.area!==null?props.area:null}
    // setter={props.handleSetState}
    placeholder={'Area'}
    />
    <SearchField 
    text={props.centroid}
    // setter={props.handleSetCountry}
    placeholder={'Centroid:'}
    />
    <div style={{display:'flex',flexDirection:'row',marginBottom:"1vh"}}>
        <SearchButton 
        text={"Close"}
        action={props.handleSetDrawPolyOpen}
        />
        <span style={{width:'1vw'}}/>
        <SearchButton 
        text={"Clear"}
        action={props.handleClearPolygon}
        />
        <span style={{width:'1vw'}}/>
        <SearchButton 
        text={"Draw"}
        action={props.handleDrawPolygon}
        />
        <span style={{width:'1vw'}}/>
        <SearchButton 
        text={"Set"}

        action={props.handleSetPolygon}
        />
        </div>
  </div>
  </>
}
  </div>

)
}

export const clearPolygonTemplate={
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {
        "category": null
      },
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              0.0,
              0.0
            ],
            [
              0.1,
              0.1
            ],
            [
              0.0,
              0.0
            ]
          ]
        ]
      }
    }
  ]
}
