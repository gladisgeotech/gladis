import React from "react";
import './styles.css'

export const GladisText= (props) => {
return(
<>
<p class='gladisText'> 
There are many  undervalued applications for real time GPS tracking and Geofencing technology for individuals and small businesses involved in outdoor recreation.
<br></br><br></br>
For example, if I am a hunter,
<br></br><br></br>
-I want to know the location of my friends, my equipment and my vehicle, in case I become lost or low visibility becomes an issue
<br></br><br></br>
-I  want to know where property lines are, so I don’t trespass or wander into a dangerous area
<br></br><br></br>
-Also, if  I make shot, I want to know that all of my friends are safely outside of my line of fire
<br></br><br></br>
Or what if I’m hosting a large scale outdoor event, like a  mountain bike race, a trail run, or in my case...a scenario paintball event
<br></br><br></br>
-I want to track and monitor all of my participants in real time
<br></br><br></br>
-I want to control the boundaries of my venue so I can  be alerted if a participant goes out of bounds.
<br></br><br></br>
-Most of all, I want to share my live tracking data with the world, to help promote my sport  and grow the event.
<br></br><br></br>
To do all of this , I will need a LOT of trackers, possibly numbering into the hundreds or thousands
<br></br><br></br>
And in each of  these scenarios, I may be MILES away from the nearest data connection.
<br></br><br></br>
Regrettably, most existing real time tracking platforms operate over cellular data  networks and require the consumer to pay an ongoing subscription fee for each device
<br></br><br></br>
These factors make them useless for offline applications and prohibitively expensive when more than a handful of trackers are needed.
<br></br><br></br>
GLADIS is different. 
<br></br><br></br>
Originally designed for use in the sport of Scenario Paintball, GLADIS is a real time tracking platform which is affordable, scalable ,and easy to use.
<br></br><br></br>
Best of all, is capable of operating 100% offline, with NO need for a data connection or subscription fee.
<br></br><br></br>
The target market for GLADIS will be individuals, groups and businesses involved in outdoor recreation, special events and search and rescue.
<br></br><br></br>
 I believe the factors that make GLADIS unique will also make it the go-to solution for these and many other off the grid applications.
</p>
<h3 onClick={props.handleSetGladisMore}style={{color:'white', marginTop:'0vh'}}>Close</h3>
</>
)
}