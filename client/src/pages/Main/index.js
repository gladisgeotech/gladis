import React, { Component, useState, useContext, useEffect } from "react";
import { DataContext } from "../../common/DataContext/index";
import logo from '../../static/gladisLogo.svg';


import './styles.css'
import { Header } from '../../components/Header';
import { Footer } from '../../components/Footer';
import {fetcher,poster} from '../../calls/calls'


export const Main = () => {

  // useEffect(() => {
  //   let outpack={
  //     place_name:"Natchez,MS"
  //   }
  //   poster(outpack,'http://localhost:5000'+"/time").then((response)=>
  //     console.log(response)
  // )
  // }, []);

  return (
        <>
        <Header/>
        <div class = 'main' >
        <img class='splashLogo' src={logo}/>
        <h1 class ="title">GLADIS GEOTECH</h1>
        <h1 class ="sub-title">CUTTING THE CORDS ON REAL-TIME TRACKING-</h1>
        </div >
        <Footer/>
        </>
    );
}