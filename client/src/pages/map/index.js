import React, { Component, useState, useContext, useEffect } from "react";
import { DataContext } from "../../common/DataContext/index";
import './styles.css'
import { Header } from '../../components/Header';
import { Footer } from '../../components/Footer';
import { MapWidget } from "./mapWidget";




export const MapPage = () => {

  return (
        <>
        <Header/>
        <div class = 'mapmain'>
          {/* <div style={{width:'100vw',height:'100vw',backgroundColor:'blueGrey'}}> */}
            <MapWidget/>
          {/* </div> */}
        </div>
        <Footer/>
        
        </>
    );
}