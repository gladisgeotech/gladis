import React,{useState} from "react";
import up_arrow from '../../static/up_chevron_white.png';
import down_arrow from '../../static/down_chevron_white.png';
import './styles.css';

export const ServicesText = () => {

const[mapSectionOpen,setMapSectionOpen]=useState(false)
const[kmlSectionOpen,setKmlSectionOpen]=useState(false)
const[tagSectionOpen,setTagSectionOpen]=useState(false)
const[vectorSectionOpen,setVectorSectionOpen]=useState(false)


const handleSetMapSectionOpen=()=>{
  setMapSectionOpen(!mapSectionOpen)
}
const handleSetKmlSectionOpen=()=>{
  setKmlSectionOpen(!kmlSectionOpen)
}
const handleSetTagSectionOpen=()=>{
  setTagSectionOpen(!tagSectionOpen)
}
const handleSetVectorSectionOpen=()=>{
  setVectorSectionOpen(!vectorSectionOpen)
}

const SectionExpand=(props)=>{
  return(
    <div onClick={props.action} style={{width:'2vw'}}>
    <img
    class='arrow'
      src={props.section? up_arrow:down_arrow}
    >
    </img>
    </div>
  )
  }

const MapText=()=>{
  return(
  <p style={{color:'black', paddingLeft:'1vw', paddingRight:'1vw', paddingTop:'0vh'}}>
  Gladis Geotech offers custom physical & digital map design & production using the highest quality officaially liscences satellite imagery available.
  <br></br>
  <br></br>
  Our physical maps are printed on a nearly indestructable polymer substrate paper known as Terraslate, which is manufactured right here in Colorado
  <br></br>
  <br></br>
  Terraslate is rip proof, water proof, chemical & heat resistant and will maintain it's shape and integrety despite being rolled, crumpled or shoved in a pocket.
  <br></br>
  <br></br>
  Applications include: 
  <br></br>
  <br></br>
  Hiking, Biking, Hunting, Off-roading, Races, Tours, Special events
</p>
  )
}
const ServiceRow = (props)=>{
  return(
    <>
    <div style={{width:'42%', backgroundColor:'lightgrey',marginBottom:'8vh'}}>
    <div style={{height:'5vh',display:'flex',flexDirection:'row',justifyContent:'center',alignItems:'center',textAlign:'center', backgroundColor:'darkgrey'}}>
    <h2 style={{paddingTop:'0vh',color:"white",textAlign:'center', marginRight:'0vh',paddingRight:'0vh'}}>{props.title}</h2>
    <SectionExpand 
      section={props.section}
      action={props.handler}
    />
    </div>
    {props.section ?
    <>
    {props.text}
    <h5 style={{color:'black', paddingLeft:'1vw', paddingRight:'1vw', paddingTop:'0vh'}}>See Examples</h5>
    <h5 style={{color:'black', paddingLeft:'1vw', paddingRight:'1vw', paddingTop:'0vh'}}>Get your free quote now.</h5>
    </>
    :
    <></>
    }
    </div>
    </>
  )
}



return (
    <>
    <div style={{height:'auto',width:'80%',margin:'auto',display:'flex',flexDirection:'column',alignItems:'center',marginTop:'8vh',marginBottom:'12vh'}}>
      <h1 style={{paddingTop:'1vh',color:"white",textAlign:'center',marginBottom:'4vh'}}>Services</h1>
      
      <ServiceRow 
        title={"Satellite & Aerial Imagery Map Design & Production"} 
        text = {<MapText/>}
        section={mapSectionOpen}
        handler={handleSetMapSectionOpen}
      />


      <ServiceRow 
        title={".kml/.kmz Design & 3-D Modeling"} 
        text = {<MapText/>}
        section={kmlSectionOpen}
        handler={handleSetKmlSectionOpen}
      />

      <ServiceRow 
        title={"Geotagged Media Processing"} 
        text = {<MapText/>}
        section={tagSectionOpen}
        handler={handleSetTagSectionOpen}
      />

      <ServiceRow 
        title={"Custom Vector Graphics & Logo Design"} 
        text = {<MapText/>}
        section={vectorSectionOpen}
        handler={handleSetVectorSectionOpen}
      />
    </div>
    </>
  )

}
