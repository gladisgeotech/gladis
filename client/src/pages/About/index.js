import React from "react";
import { Header } from '../../components/Header';
import { Footer } from '../../components/Footer';
import { AboutUsText } from "./aboutUsText";
import './styles.css';

export const About = () => {

  return (
    <>
      <Header/>
        <div class = 'aboutmain'>
        <div class = 'textDiv'>
        <AboutUsText/>
        </div>
            

        </div>
      <Footer/> 
    </>
    );
}