import React from "react";
import logo from '../../static/gladisLogo.png';
import { NavLink } from "react-router-dom";
import styled from "styled-components";
import './styles.css'

const AppBar = styled.header`
  font-family: "Hind Guntur", sans-serif;
  font-size: 14px;
  line-height: 1.4285em;
  transition: box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2),
    0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12);
  width: 100%;
  display: flex;
  z-index: 1100;
  box-sizing: border-box;
  // flex-shrink: 0;
  flex-direction: row;
  position: static;
  color: rgba(0, 0, 0, 0.87);
  background-color: #050505;
  margin-bottom:0;
`;


const NavImg=()=>{
return(
  <img
  class='navImg'
    src={logo}
  >
  </img>
)
}

const Nav_Link=(props)=>{
  return(
  <h6 class ='headerlink'>
    {props.text}
  </h6>
  )
  }
export const Header = () => {
  return (
    <AppBar>
        <NavImg />
        <NavLink to="/"style={{marginLeft:'1vw',color:"whitesmoke",textDecoration: 'none'}}>
          <Nav_Link text={"HOME"}/>
        </NavLink>
        <NavLink to="/about"style={{color:"whitesmoke",textDecoration: 'none'}}>
          <Nav_Link text={"ABOUT US"}/>
        </NavLink>
        <NavLink to="/projects"style={{color:"whitesmoke",textDecoration: 'none'}}>
          <Nav_Link text={"PROJECTS"}/>
        </NavLink>
        <NavLink to="/map"style={{color:"whitesmoke",textDecoration: 'none'}}>
          <Nav_Link text={"MAP"}/>
        </NavLink>
        <NavLink to="/contact"style={{color:"whitesmoke",textDecoration: 'none'}}>
          <Nav_Link text={"CONTACT"}/>
        </NavLink>
    </AppBar>
  );
};


        // {/* <NavLink to="/services"style={{color:"whitesmoke",textDecoration: 'none'}}>
        //   <Nav_Link text={"SERVICES"}/>
        // </NavLink> */}