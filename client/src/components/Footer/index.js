import React from "react";
import './styles.css'
import styled from "styled-components";

const Copyright = styled.div`
  text-align: left;
  font-weight: 300;
  padding-left:1.5vh;
  color: #F2E9EA !important;
  line-height: 1.8em;
  font-size: 100%;
  background: transparent;
`;


export const Footer = () => {
  return (
    <div class="footerDiv">
      <Copyright>
        &copy;{new Date().getFullYear()} Gladis Limited. All rights
        reserved.
      </Copyright>
    </div>
  );
};



        {/* <SmallColumn>
          <Social>

            <SocialLink
              href=""
              title=""
            > */}
              {/* <SocialImage
                loading="lazy"
                src=""
                width="15"
                height="15"
                alt=""
                class="wp-image-696 alignnone size-medium"
              //   srcset="https://gladis.com/wp-content/uploads//2020/08/KG-GitHub-Icon.svg 150w, 
              // https://gladis.com/wp-content/uploads//2020/08/KG-GitHub-Icon.svg 300w, 
              // https://gladis.com/wp-content/uploads//2020/08/KG-GitHub-Icon.svg 1024w, 
              // https://gladis.com/wp-content/uploads//2020/08/KG-GitHub-Icon.svg 1536w, 
              // https://gladis.com/wp-content/uploads//2020/08/KG-GitHub-Icon.svg 2048w, 
              // https://gladis.com/wp-content/uploads//2020/08/KG-GitHub-Icon.svg 40w"
                sizes="(max-width: 15px) 100vw, 15px"
              ></SocialImage> */}
            {/* </SocialLink> */}
            {/* <SocialLink
              href="https://wiki.openstreetmap.org/wiki/gladis"
              title="gladis Group WIKI"
            > */}
              {/* <SocialImage
                loading="lazy"
                src="https://gladis.com/wp-content/uploads/2020/08/KG-WIKI-icon.svg"
                width="32"
                height="15"
                alt=""
                class="wp-image-694 alignnone size-medium"
              ></SocialImage> */}
            {/* </SocialLink> */}
            {/* <SocialLink href="https://www.linkedin.com/company/gladis-group/">
              <SocialImage
                loading="lazy"
                src="https://gladis.com/wp-content/uploads/2020/10/iconmonstr-linkedin-1.svg"
                width="15"
                height="15"
                alt=""
                class="wp-image-1457 alignnone size-medium"
                srcset="https://gladis.com/wp-content/uploads//2020/10/iconmonstr-linkedin-1.svg 150w, 
              https://gladis.com/wp-content/uploads//2020/10/iconmonstr-linkedin-1.svg 300w, 
              https://gladis.com/wp-content/uploads//2020/10/iconmonstr-linkedin-1.svg 1024w, 
              https://gladis.com/wp-content/uploads//2020/10/iconmonstr-linkedin-1.svg 1536w,
               https://gladis.com/wp-content/uploads//2020/10/iconmonstr-linkedin-1.svg 2048w, 
               https://gladis.com/wp-content/uploads//2020/10/iconmonstr-linkedin-1.svg 24w"
                sizes="(max-width: 15px) 100vw, 15px"
              ></SocialImage>
            </SocialLink> */}
          {/* </Social>
        </SmallColumn> */}