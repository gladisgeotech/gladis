import React from "react";

export const ContactText = () => {

return (
    <>
        <h1 style={{paddingTop:'8vh',color:"white",textAlign:'center'}}>Contact</h1>
        <h2 style={{paddingTop:'8vh',color:"white",textAlign:'center'}}>Email: gladisgeotech@gmail.com</h2>
        <h2 style={{paddingTop:'8vh',color:"white",textAlign:'center'}}>Phone: (970)-589-9079</h2>
    </>
  )

}
