import React from "react";

export const AboutUsText = () => {

return (
    <>
        <h1 style={{paddingTop:'8vh',color:"white",textAlign:'center'}}>About Us</h1>
        <p style={{fontSize:'1em',color:'white', marginLeft:'4vw',marginRight:'4vw',marginBottom:'8vh',padding:'auto',paddingBottom:'10vh',textAlign:'left'}}> 
        Hello, my name is Chris Gousset, founder of Gladis Geotech. "TEST"
        <br></br>
        <br></br>
        Five years ago, I began work on a secret side project now known as GLADIS, the Geofencing Live Asset Display and Interface System.
        <br></br>
        <br></br>
        The name GLADIS is a tribute to mathemetician and scientist Dr. Gladys West.
        <br></br>
        <br></br>
        Her pioneering work in satellite geodesy and radar altimetry during the 1970’s and 1980’s paved the way for modern GPS technology, and none of the mapping and navigation software we take for granted today would be possible without her invaluable contributions to what later became the Geotech field.
        <br></br>
        <br></br>
        She is a great inspiration for me personally, if you have never heard of her, I strongly encourage you to look her up when you leave here and read her story.
        <br></br>
        <br></br>
        I started work on GLADIS because I believe that there are a multitude of applications for real time GPS tracking and geofencing technology that are underutilized or often entirely unexplored, simply because most existing real-time tracking platforms available on the market fail to suit the needs of so very many end users who could greatly benefit from this technology.
        <br></br>
        <br></br>
        You see, I am myself an avid scenario paintball player and producer of my own large scale paintball events.
        <br></br>
        <br></br>
        To give you some context, Scenario paintball can be thought of as themed and structured paintball on a grand scale, usually involving hundreds if not thousands of players, broken up into two or more teams, and usually taking place over the course of several days.
        <br></br>
        <br></br>
        After a game one day, some of my teammates and I started talking about all of the ways we could use real time GPS tracking tech to our advantage on the field.
        <br></br>
        <br></br>
        In the weeks and months following, I also started thinking about all of the ways I wanted to use the same technology to enhance the quality, safety, and the entertainment value of the events I hosted.
        <br></br>
        <br></br>
        I wanted to be able to track several hundred or even thousands of participants and all of my game staff in real time.
        <br></br>
        <br></br>
        I also wanted to easily set up and customize geofences to control the boundaries of my playing field, so I could be alerted if a player goes out of bounds, as well as to monitor and automate the “dead zones” where eliminated players go to wait to re-enter the game at predetermined times.
        <br></br>
        <br></br>
        Most of all though, I wanted a tracking system that could operate anywhere I went, since many scenario paintball events are hosted in remote locations, far away from the nearest data connection.
        <br></br>
        <br></br>
        However, to my great disspointment, when I looked around for a GPS tracking platform we could use, I very quickly found that there were none available that met all of our needs.
        <br></br>
        <br></br>
        It turns out, quite regrettably, that the majority of existing gps tracking platforms operate exclusively over cellular data networks and require the consumer to pay an ongoing subscription fee for each tracking device.
        <br></br>
        <br></br>
        Many also require the consumer to provide their own hardware, usually in the form of a smartphone or table, to use as an interface.
        <br></br>
        <br></br>
        In one way or another, the core functionality of these products is locked away behind a subscription fee or secondary hardware requirement.
        <br></br>
        <br></br>
        These factors make them useless for offline applications and prohibitively expensive for most consumers like myself, when more than a handful of trackers are needed.
        <br></br>
        <br></br>
        So I began work on GLADIS - as a means to empower outdoor enthusiasts and event coordinators with the ability to use GPS tracking and geofencing technology where and how we they want to use it.
        <br></br>
        <br></br>
        GLADIS is built differently.
        <br></br>
        <br></br>
        Not only is GLADIS capable of operating 100% offline, using preloaded maps and long range radio, it is also a truly self contained system that provides everything the consumer needs to start using the product, right out of the box.
        <br></br>
        <br></br>
        The GLADIS platform will also offer a higher refresh rate, longer battery life, greater interfacing and transmission flexibility, and a wider range of expansion and customization options than similar platforms.
        <br></br>
        <br></br>
        Work on GLADIS has progressed steadily over the years, and in early 2022 I founded Gladis Geotech to support the final phases of development and to bring the GLADIS platform to market.
        <br></br>
        <br></br>
        GLADIS is currently in the pre-seed phase, as I seek funding to complete and field test the GLADIS MVP.
        <br></br>
        <br></br>
        However, the GLADIS proof of concept prototype has been finished for some time, and thorough testing has yielded very exciting results.
        <br></br>
        <br></br>
        With all of the support, feedback and encouragement I have received along the way, I truly believe I can take GLADIS to the next level,
        <br></br>
        <br></br>
        So please, stay tuned, stay in touch, and I look forward to showing you all just what GLADIS can do for you!
        <br></br>
        <br></br>
        -Chris Gousset
        <br></br>
        <br></br>
        Gladis Geotech
        <br></br>
        <br></br>
        </p>
    </>
  )

}
