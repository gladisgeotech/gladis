import React,{useState}from "react";
// import { ReactComponent as gladislogo } from '../../static/gladisLogo.svg';
// import { ReactComponent as nudgelogo } from '../../static/nudgeIconWhite.svg';
import gladislogo from '../../static/gladisLogo.svg';
import nudgelogo from '../../static/nudgeIconWhite.svg';
import { Header } from '../../components/Header';
import { Footer } from '../../components/Footer';
import { GladisText } from './gladisText';
import { NudgeText } from './nudgeText';
import './styles.css'

export const Projects = () => {

  const[gladisMore,setGladisMore]=useState(false)
  const[nudgeMore,setNudgeMore]=useState(false)

  const handleSetGladisMore=()=>{
    setGladisMore(!gladisMore)
  }
  const handleSetNudgeMore=()=>{
    setNudgeMore(!nudgeMore)
  }
  return (
    <>
      <Header/>
        <div class = 'projectsmain'>

          <div class='contentDiv'>

            <div class='gladisDiv'>
              <div class='imgDiv'>
                <img class='GladisLogo' alt={'gladis logo'} src={gladislogo}/>
              </div>
              <div class='titleDiv'>
                <h1 class='GladisTitle'>Geofencing Live Asset Display & Interface System</h1>
                <h2 class='GladisTagline'>-Cutting the cords on real-time GPS tracking</h2>
                <div class='GladisTextDiv'>

                <h3 class='GladisTagline' onClick={handleSetGladisMore}>Learn more...</h3>
                { !gladisMore?<></>
                :
                  <GladisText 
                    handleSetGladisMore={handleSetGladisMore}
                  />
                  }
                </div>
              </div>
            </div>

            <div class='nudgeDiv'>
              <div class='nudgeTitleDiv'>
                  <h1 class='GladisTitle'>NUDGE - Multi-tool for geotagged media</h1>
                  <h2 class='GladisTagline'>-Import, view, adjust and export geotagged media files.</h2>
                  <h3 class='GladisTagline' onClick={handleSetNudgeMore}>Learn more...</h3>
                  { !nudgeMore?<></>
                :
                  <NudgeText 
                    handleSetNudgeMore={handleSetNudgeMore}
                  />
                  }
              </div>
              <div class='nudgeImgDiv'>
                <img class='NudgeLogo' src={nudgelogo}/>
              </div>

            </div>
          </div>
        </div>
      <Footer/>
    </>
    );
}