from flask import Flask, jsonify
import time
from flask_restx import Resource, Api
from flask_cors import CORS, cross_origin
 
app = Flask(__name__)
api = Api(app)
cors = CORS(app, resources={r"/*": {"origins": "*"}})



# @api.route('/hello')
# class HelloWorld(Resource):
#     def get(self):
#         return {'hello': 'world'}
 
@app.route('/time', methods = ['GET', 'POST'])
def get_current_time():
    return jsonify({'time': time.time()})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)

























# from cgi import test
# import time
# from flask import Flask, send_from_directory,jsonify,render_template,request
# from flask_restful import Api, Resource, reqparse
# from flask_cors import CORS, cross_origin
# import requests
# import geojson
# # from shapely.geometry import Polygon
# app = Flask(__name__)
# # geod = Geod(ellps="WGS84")
# # CORS(app) #comment this on deployment
# api = Api(app)
# cors = CORS(app, resources={r"/*": {"origins": "*"}})
# # @app.errorhandler(404)
# # def not_found(e):
# #     return app.send_static_file('index.html')


# # @app.route('/')
# # def index():
# #     return render_template('index.html')

# @app.route('/darkmap')
# def darkmap():
#     print("test")
#     # return('test')
#     return render_template("lightmap.html")

# @app.route('/api/time', methods = ['GET', 'POST'])
# def get_current_time():
#     return jsonify({'time': time.time()})

# # @app.route('/api/polygonInfo', methods = ['GET', 'POST'])
# # def get_polygon_info():
# #     check_polygon=request.json.get("polygon")
# #     check_array=[]
# #     for cord in check_polygon:
# #         cord=(cord[0],cord[1])
# #         check_array.append(cord)
# #     check_polygon=Polygon(check_array)
# #     # area = abs(geod.geometry_area_perimeter(check_polygon)[0])
# #     centroid=check_polygon.centroid
# #     centroid=str(centroid).split('POINT ')[1]
# #     centroid=centroid.replace("(","").replace(")","").split(" ")
# #     centroid=[float(cord)for cord  in centroid]
# #     print(centroid)
    

#     return jsonify({'area': area,'centroid':centroid})

# @app.route('/api/geocoder', methods = ['GET', 'POST'])
# def geocoder_route():
#     city=request.json.get("city").capitalize()
#     address=request.json.get("address").capitalize()
#     postcode=request.json.get("postcode").capitalize()
#     state=request.json.get("state").capitalize()
#     country=request.json.get("country").capitalize()
#     args=""
#     if city != "None":
#         args+=("city=%s&"%(city))
#     if address != "None":
#         args+=("street=%s&"%(address))
#     if postcode != "None":
#         args+=("postalcode=%s&"%(postcode))
#     if state != "None":
#         args+=("state=%s&"%(state))
#     if country != "None":
#         args+=("country=%s&"%(country))
#     headers={
#         "mode":"cors",
#         "Accept-Language":"en-US",
#     }
    
#     search_url="https://nominatim.openstreetmap.org/search?%sformat=geojson"%(args)
#     search_results=requests.request("GET",search_url,headers=headers).json()
#     return_array=[]
#     polygon=[]
#     props_obj={'polygon':None,'search_center':None}
#     first_feature=search_results['features'][0]['geometry']['coordinates']
#     for feature in search_results['features']:
#         return_obj={}
#         return_obj['index']=search_results['features'].index(feature)
#         return_obj['name']=feature['properties']['display_name']
#         return_obj['coordinates']=feature['geometry']['coordinates']
#         polygon.append(feature['geometry']['coordinates'])
#         return_array.append(return_obj)
#     print(return_array)
#     if len(search_results['features'])>1:    
#         polygon.append(first_feature)
#         check_poly=Polygon(polygon)
#         check_center=check_poly.centroid.coords[0][0]
#         return_center=[check_poly.centroid.coords[0][0],check_poly.centroid.coords[0][1]]
#         props_obj['polygon']=polygon
#         props_obj['search_center']=return_center

#     # search_results=geojson.FeatureCollection(search_results)
#     obj={'results':return_array,'properties':props_obj}

#     return jsonify(obj)

# if __name__ == '__main__':
#     app.run(host='0.0.0.0', port=80)