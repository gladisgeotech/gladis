// import { API_URL } from "./components/constants";
// import Cookie from "js-cookie";
var obj;

//--------------poster----------
export async function poster(info, route) {
  const response = await fetch(route, {
    method: "POST",
    body: JSON.stringify(info),
    headers: {
      "Content-Type": "application/json",
      // mode: "cors"
    },
  });
  if (response.ok) {
    obj = await response.json();
  } else if (!response.ok) {
    obj = { response: "error" };
  }
  return obj;
}

//--------------fetcher---------
export async function fetcher(route) {
  const response = await fetch(route, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      // mode: "cors",
    },
  });
  if (response.ok) {
    obj = await response.json();
  } else if (!response.ok) {
    obj = { response: "Error" };
  }
  console.log(obj)
  return obj;
}


// "X-CSRF-TOKEN": `${Cookie.get("csrf_access_token")}`,

// "X-CSRF-TOKEN": `${Cookie.get("csrf_access_token")}`,